$(document).ready(function() {
    $(window).bind('scroll', function(e) {
        parallax();
    });
});

function parallax() {
    var scrollPosition = $(window).scrollTop();
    $('#para').css('top',(0 - (scrollPosition * .7))+'px' );
    $('#para1').css('top',(0 - (scrollPosition * .1))+'px' );
    $('#para2').css('top',(0 - (scrollPosition * .4))+'px' );
}       