
      
  // function to show Amenities on map
$(document).on("click",".amenity_sidebar",function()
{ 
         if($(this).hasClass("amenity_check"))
          {
              $(this).removeClass("amenity_check");
          }        
          else
          {
             $(this).addClass("amenity_check");
          }
        
         
         var svg_string = svg_string_data;
         
         amenities = new Array();
         var k=0;
         var amenity_array = svg_string.split(" ");
          for(i=0;i<amenity_array.length;i++)
          {
            if(amenity_array[i].includes('id="poi_'+this.id))
              {
                var id = amenity_array[i].split("\"");
                amenities[k] = id[1];
                k++;
                
              }
          }

          for(i=0;i<amenities.length;i++)
          {
            
            var amenity = document.getElementById(amenities[i]);
            

            if(amenity.tagName != "image")
            {
              svg = document.createElementNS('http://www.w3.org/2000/svg','svg');
              svg.setAttribute("width","100");
              svg.setAttribute("height","100");
              def = document.createElementNS('http://www.w3.org/2000/svg','defs');
              var txt = document.createElementNS('http://www.w3.org/2000/svg','image');
              txt.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', "/amenities/"+this.id+".png");
              if(amenity.getAttribute("points"))
              {
                 points = amenity.getAttribute("points").split(" ");
                 txt.setAttribute("x",points[0]-20);
                 txt.setAttribute("y",points[1]+25);
              }
              else
              {
                txt.setAttribute("x",amenity.getAttribute("x"));
                txt.setAttribute("y",amenity.getAttribute("y"));
              }
              
              // txt.setAttribute("font-family","FontAwesome");
              // txt.setAttribute("font-size","40px");
              // txt.textContent = '\uf040';
              //txt.setAttribute("xlink:href","/assets/fountain.png");
              txt.setAttribute("id",amenities[i]);
              txt.setAttribute("visibility","visible");
              txt.setAttribute("height","50");
              txt.setAttribute("width","50");
              def.appendChild(txt)
              svg.appendChild(def);
              amenity.parentNode.replaceChild(txt, amenity);

          }
          else
          {
            if(amenity.getAttribute("visibility") == "visible")
            {  
              
              amenity.setAttribute("visibility","hidden"); 
            }
            else
            {
              
              amenity.setAttribute("visibility","visible");
            } 
          }
      }


});
    
